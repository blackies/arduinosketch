#include <SPI.h>
#include <Wire.h>
#include <GridEye.h>

/*
 * Macro Definitions
 */
#define SPEC_TRG         A0 // C12880 TRG signal
#define SPEC_ST          A1 // C12880 ST signal
#define SPEC_CLK         A2 // C12880 CLK signal
#define SPEC_VIDEO       A3 // C12880 VIDEO signal
//#define WHITE_LED        A4 // We do not use WHITE LED

#define C12880_SPEC_CHANNELS    288 // New Spec Channel for C12880
#define GRIDEYE_DATA_SIZE       64 // Number of pixels of GridEye

#define DEBUG 0   // DEBUG switch
#define OUTPUT_SERIAL 0 // OUTPUT type - Serial
#define OUTPUT_SPI    1 // OUTPUT type - SPI
#define OUTPUT_TYPE OUTPUT_SPI // Used OUTPUT (Serial for debugging, SPI for release)

GridEye gridEye; // GridEye instance
int thermTemp; // GridEye thermistor temperature
uint16_t c12880Data[C12880_SPEC_CHANNELS]; // Data from C12880 - array of measured values
int gridEyeData[GRIDEYE_DATA_SIZE]; // Data from GridEye - array of measured values
  
int c12880BytePosition = 0; // current index of transferred C12880 data (SPI)
bool c12880FirstByte = true; // reading first byte of 2-bytes?
int gridEyeBytePosition = 0; // current index of gridEye data (SPI)
bool gridEyeFirstByte = true; // reading first byte of 2-bytes?

void setup(){

  //Set desired pins to OUTPUT
  pinMode(SPEC_CLK, OUTPUT);
  pinMode(SPEC_ST, OUTPUT);
  //pinMode(LASER_404, OUTPUT);
  //pinMode(WHITE_LED, OUTPUT);

  digitalWrite(SPEC_CLK, HIGH); // Set SPEC_CLK High
  digitalWrite(SPEC_ST, LOW); // Set SPEC_ST Low

  // I2C initialization
  Wire.begin();

  if (OUTPUT_TYPE == OUTPUT_SERIAL || DEBUG) {
    Serial.begin(115200); // Baud Rate set to 9600
  }
  if (OUTPUT_TYPE == OUTPUT_SPI) {
  
    // have to send on master in, *slave out*
    pinMode(MISO, OUTPUT);
    // turn on SPI in slave mode
    SPCR |= _BV(SPE);
    // turn on interrupts
    SPI.attachInterrupt();

    // init counters
    c12880FirstByte = true;
    c12880BytePosition = 0;
    gridEyeFirstByte = true;
    gridEyeBytePosition = 0;
  }
}

// SPI interrupt routine
ISR (SPI_STC_vect)
{
  byte c = SPDR; // read control byte from SPI
  if (DEBUG) Serial.print(c);
  if (c == 0x01) { // beginning - spectrometer    
    if (DEBUG) Serial.println("C12880 beginning. Running calculation.");
    readC12880();
    delayMicroseconds(1000); // delay to be sure that data are set
    if (DEBUG) printGridEyeData();
    c12880BytePosition = 0; // reset counters
    c12880FirstByte = 1;
    SPDR = 0xFF; // return control char 0xFF
  } else if (c == 0xFF) { // data request for C12880 data
     if (c12880BytePosition >= C12880_SPEC_CHANNELS) {
      SPDR = c; // reading finished, return 0xFF
      return;
     }
     if (c12880FirstByte) { 
      SPDR = c12880Data[c12880BytePosition]/256; // return first byte of c12880 data (index: c12880BytePosition)
      c12880FirstByte = false; // next request will return second byte
     } else {
      SPDR = c12880Data[c12880BytePosition]%256; // return second byte of c12880 data (index: c12880BytePosition)
      c12880BytePosition++; // increment index position
      c12880FirstByte = true; // next request will return first byte
     }
  } else if (c == 0x02) { // beginning - gridEye
    if (DEBUG) Serial.println("GridEye beginning.");
    gridEyeBytePosition = 0; // reset counters
    gridEyeFirstByte = 1;
    SPDR = 0xFE; // return control char 0xFE
  } else if (c == 0xFE) {    // data request for GridEYE data
    if (gridEyeBytePosition >= GRIDEYE_DATA_SIZE) {
      SPDR = c;
      return;
    }
    if (gridEyeFirstByte) {
      SPDR = gridEyeData[gridEyeBytePosition]/256; // return first byte of GridEYE data (index: c12880BytePosition)
      gridEyeFirstByte = false; // next request will return second byte
    } else {
      SPDR = gridEyeData[gridEyeBytePosition]%256; // return second byte of GridEYE data (index: c12880BytePosition)
      gridEyeBytePosition++; // increment index position
      gridEyeFirstByte = true; // next request will return first byte
    }
  }
}  

/* 
 * Read GridEye values and therm. temperature 
 */
void readGridEye() {
  thermTemp = gridEye.thermistorTemp(); // read thermistor temperature
  // Pixel temperature data read
  gridEye.pixelOut(gridEyeData); // read GridEYE data
}

/*
 * Print GridEye data (via Serial)
 */
void printGridEyeData() {
  Serial.print(F("Thermistor Temp: "));
  Serial.println(thermTemp * 0.065); // 1 unit = 0.065 degrees

  Serial.println(F("Pixel Output: "));
  for (int i = 0; i < 64; i++) {
    if (i && ((i % 8) == 0)) {
      Serial.println();
    }
    Serial.print(gridEyeData[i] * 0.25); // 1 unit = 0.25 degrees
    Serial.print(' ');
  }
  Serial.println();
}

/*
 * This function reads spectrometer data from SPEC_VIDEO
 */
void readC12880(){

  int delayTime = 1; // delay time

  // Start clock cycle and set start pulse to signal start
  digitalWrite(SPEC_CLK, LOW);
  delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, HIGH);
  delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, LOW);
  digitalWrite(SPEC_ST, HIGH);
  delayMicroseconds(delayTime);

  //Sample for a period of time
  for(int i = 0; i < 15; i++){
      digitalWrite(SPEC_CLK, HIGH);
      delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
      delayMicroseconds(delayTime); 
  }

  //Set SPEC_ST to low
  digitalWrite(SPEC_ST, LOW);

  //Sample for a period of time
  for(int i = 0; i < 85; i++){
      digitalWrite(SPEC_CLK, HIGH);
      delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
      delayMicroseconds(delayTime); 
  }

  //One more clock pulse before the actual read
  digitalWrite(SPEC_CLK, HIGH);
  delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, LOW);
  delayMicroseconds(delayTime);

  //Read from SPEC_VIDEO
  for(int i = 0; i < C12880_SPEC_CHANNELS; i++){
      c12880Data[i] = analogRead(SPEC_VIDEO);
      
      digitalWrite(SPEC_CLK, HIGH);
      delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
      delayMicroseconds(delayTime);
  }

  //Set SPEC_ST to high
  digitalWrite(SPEC_ST, HIGH);

  //Sample for a small amount of time
  for(int i = 0; i < 7; i++){
      digitalWrite(SPEC_CLK, HIGH);
      delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
      delayMicroseconds(delayTime);
  }
  digitalWrite(SPEC_CLK, HIGH);
  delayMicroseconds(delayTime);
}

/*
 * The function below prints out data to the terminal or 
 * processing plot
 */
void printC12880Data(){
  for (int i = 0; i < C12880_SPEC_CHANNELS; i++){
    Serial.print(c12880Data[i]);
    Serial.print(',');
  }
  Serial.println("\n");
}

/*
 * loop function
 */
void loop(){   
  if (DEBUG || OUTPUT_TYPE == OUTPUT_SERIAL) {
    readC12880();
    printC12880Data();  
    readGridEye();
    printGridEyeData();
     
    delay(5000);
  } else if (OUTPUT_TYPE == OUTPUT_SPI) {
    readGridEye(); // have to read data periodically, due to long delay
    delay(50000);
  }
}
